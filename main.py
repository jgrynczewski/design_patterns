from indicators import Indicators_Data

# Report on current indica opentors values
for indicator in Indicators_Data:
    if indicator.name == "open":
            print(f"Current open tickets {indicator.value}")
    elif indicator.name == "assigned":
            print(f"Assigned tickets {indicator.value}")
    elif indicator.name == "closed":
            print(f"Tickets closed in last hour {indicator.value}")