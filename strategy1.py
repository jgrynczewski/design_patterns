from strategy.order import Order
from strategy.shipping_cost import ShippingCost
from strategy.fedex_strategy import FedexStrategy
from strategy.ups_strategy import UpsStrategy
from strategy.postal_strategy import PostalStrategy

# Test Federal Express Shipping
order = Order()
strategy = FedexStrategy()
cost_calculator = ShippingCost(strategy)
cost = cost_calculator.shipping_cost(order)
assert cost == 3.0

# Test IUps Shipping
order = Order()
strategy = UpsStrategy()
cost_calculator = ShippingCost(strategy)
cost = cost_calculator.shipping_cost(order)
assert cost == 4.0


# Test Postal Shipping
order = Order()
strategy = PostalStrategy()
cost_calculator = ShippingCost(strategy)
cost = cost_calculator.shipping_cost(order)
assert cost == 5.0

print("Test passed")