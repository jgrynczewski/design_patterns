class MyClass:

    def __init__(self, value):
        self.__value = value

    @property
    def value(self):
        return self.__value

    @value.setter
    def value(self, value):
        self.__value = value

    @value.deleter
    def value(self):
        del self.__value



my_class = MyClass(5)
print(my_class.value)
my_class.value = 4
print(my_class.value)
del my_class.value
print(my_class.value)