from observer.observer_abs import AbsObserver

class Observer2(AbsObserver):
    _open_tickets = 0
    _close_tickets = 0
    _assigned_tickets = 0

    def __init__(self, subject):
        self._subject = subject
        subject.attach(self)

    def update(self):
        self._open_tickets = self._subject.open_tickets
        self._close_tickets = self._subject.close_tickets
        self._assigned_tickets = self._subject.assigned_tickets
        self.display()

    def display(self):
        print(f"Current open tickets: {self._open_tickets}")
        print(f"Assigned tickets: {self._assigned_tickets}")
        print(f"Closed tickets: {self._close_tickets}")
        print("******\n")


