from observer.subject_abs import AbsSubject

class Indicator2(AbsSubject):
    _open_tickets =  0
    _close_tickets = 0
    _assigned_tickets = 0

    @property
    def open_tickets(self):
        return self._open_tickets

    @property
    def close_tickets(self):
        return self._close_tickets

    @property
    def assigned_tickets(self):
        return self._assigned_tickets

    def set_indicators(self, open_tickets, close_tickets, assigned_tickets):
        self._open_tickets = open_tickets
        self._close_tickets = close_tickets
        self._assigned_tickets = assigned_tickets
        self.notify()

