my_list = [1, 2, 3, 4]

for item in my_list:
    print(item)
#
# for item in 3:
#     print("a")

print(iter([1,2,3]))
print(iter((1,2,3)))

# print(iter(4))

my_iterator = iter([1, 2, 3])
print(next(my_iterator))
print(next(my_iterator))
print(next(my_iterator))
# print(next(my_iterator))

my_list = [1,2,3]
for item in my_list:
    print(item)

my_list = [1, 2, 3]
my_iterator = iter(my_list)
while True:
    try:
        item = next(my_iterator)
        print(item)
    except StopIteration:
        break