class MyClass:
    def __init__(self, value):
        self.__value = value

    def getValue(self):
        return self.__value

    # setValue, delValue

    value = property(getValue, setValue, delValue )
