from abs_builder import ABCBuilder
from computer import Computer

class MyComputerBuilder(ABCBuilder):

    def new_computer(self):
        self._computer =  Computer()

    def get_computer(self):
        return self._computer

    def get_case(self):
        self._computer.case = "Coolermaster N300"

    def build_mainboard(self):
        self._computer.mainboard = "MSI 970"
        self._computer.cpu = "Intel Core i7-4470"
        self._computer.memory = "16GB"

    def install_mainboard(self):
        pass