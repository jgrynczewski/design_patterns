class Computer:

    def __init__(self, case, mainboard, cpu, memory):
        self.case = case
        self.mainboard = mainboard
        self.cpu = cpu
        self.memory = memory

    def display(self):
        print("Custom computer")
        print(f"Case {self.case}")
        print(f"Mainboard {self.mainboard}")
        print(f"cpu {self.cpu}")
        print(f"Memory {self.memory}")
