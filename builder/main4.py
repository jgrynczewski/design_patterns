from computer3 import Computer
from mycomputer4 import MyComputer

builder = MyComputer()
builder.build_computer()
computer = builder.get_computer()
computer.display()