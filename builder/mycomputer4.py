from computer3 import Computer

class MyComputer:

    def get_computer(self):
        return self._computer

    def build_computer(self):
        computer = self._computer = Computer()
        self.get_case()
        self.build_mainboard()
        self.install_mainboard()

    def get_case(self):
        self._computer.case = "Coolermaster N300"

    def build_mainboard(self):
        self._computer.mainboard = "MSI 970"
        self._computer.cpu = "Intel"
        self._computer.memory = "16GB"

    def install_mainboard(self):
        pass