from computer3 import Computer

class MyComputer:

    def get_computer(self):
        return self._computer

    def build_computer(self):
        computer = self._computer = Computer()
        computer.case = "Coolermaster N300"
        computer.mainboard = "MSI 970"
        computer.cpu = "Intel Core i7-4770"
        computer.memory = "16GB"
