from computer1 import Computer

computer = Computer(
    case = "Coolermaster N300",
    mainboard = "MSI 970",
    cpu = "Intel Core i7-440",
    memory = "16GB"
)

computer.display()